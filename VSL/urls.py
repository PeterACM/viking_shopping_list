# Peter AC Munro Python Portfolio
# Viking Shopping List
# VSL/URLs

"""VSL App URL Configuration"""

from django.urls import path
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'VSL'
urlpatterns = [
    # Homepage | Log in page
    path('', auth_views.LoginView.as_view(template_name='VSL/index.html'), name='index'),
    # Log out
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    # Events list
    path('events/', views.events, name='events'),
    # Individual Event and it's shopping list
    path('events/<int:event_id>/', views.event, name='event'),
    # Add a new Event
    path('new_event', views.new_event, name='new_event'),
    # Edit and Event
    path('edit_event/<int:event_id>', views.edit_event, name='edit_event'),
    # Delete an event and all of it's shopping list
    path('delete_event/<int:event_id>', views.delete_event, name='delete_event'),
    # Add Item to shopping list
    path('add_item/<int:event_id>', views.add_item, name='add_item'),
    # Pillage an item on the shopping list
    path('pillage/<int:item_id>', views.pillage_item, name='pillage_item'),
    # Delete an item from the shopping list
    path('delete_item/<int:item_id>', views.delete_item, name='delete_item'),
    # set as attending an event
    path('is_attending/<int:event_id>', views.is_attending, name='is_attending'),
    # set as not attending an event
    path('not_attending/<int:event_id>', views.not_attending, name='not_attending'),
    # set as attending an event by email
    path('attending_confirmed/<int:event_id>/<uidb64>/<token>/', views.attending_confirmed, name='attending_confirmed'),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)