# Peter AC Munro Python Portfolio
# Viking Shopping List
# VSL/Models

from django.db import models
from django.contrib.auth.models import User
import datetime


class Event(models.Model):
    """A thing (not a Viking Thing, just a thing), occuring at a particular time and place, to be attended"""
    title = models.CharField(max_length=50, help_text="Name of the glorious feasting and battle!")
    date_of = models.DateField(default=datetime.date.today, help_text="Date that the Battling and Feasting will be")
    date_added = models.DateTimeField(auto_now_add=True,)
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True,)

    def __str__(self):
        """Return a string representation of the model."""
        return self.title


class Attendance(models.Model):
    """Those who have notified that they will be attending an event."""
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name='attending')
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='attendants')
    is_attending = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s" % (self.event, self.user)


class Item(models.Model):
    """items making up the shopping list"""
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    name = models.CharField(max_length=150, help_text="Item to be added to shopping list.")
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='added_by')
    date_added = models.DateTimeField(auto_now_add=True,)
    is_pillaged = models.BooleanField(default=False)
    pillaged_by = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='pillaged_by')
    
