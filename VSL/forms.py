# Peter AC Munro Python Portfolio
# Viking Shopping List
# VSL/Forms

from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.forms import ModelForm

import datetime

from .models import Event

class VSLForm(forms.Form):
    item = forms.CharField(max_length=150, widget=forms.TextInput(attrs={"placeholder": "Item Name"}))


class EventForm(forms.ModelForm):

    error_messages = {'date_error': ("The date cannot be in the past."),}

    class Meta:
        model = Event
        fields = ['title', 'date_of']
        labels = {'title:': 'Event name:', 'date_of': 'Date of:'}
        widgets = {'date_of': forms.SelectDateWidget(), 'title': forms.TextInput(attrs={"placeholder": "Event Name"})}

    def clean_date_of(self):
        date_of = self.cleaned_data['date_of']
        if datetime.date.today() > date_of:
            raise forms.ValidationError(self.error_messages['date_error'], code='date_error')
        else:
            return self.cleaned_data['date_of']


