# Peter AC Munro Python Portfolio
# Viking Shopping List
# VSL/Tokens

from django.conf import settings
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six


class ComsOptTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (six.text_type(user.pk) + six.text_type(timestamp) + six.text_type(user.opt_in.recieve_coms))

coms_opt_token = ComsOptTokenGenerator()


class AttendTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, attend, timestamp):
        user = attend.user
        is_attending = attend.is_attending
        return (six.text_type(user.pk) + six.text_type(timestamp) + six.text_type(is_attending))

attend_token = AttendTokenGenerator()

