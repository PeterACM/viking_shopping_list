# Peter AC Munro Python Portfolio
# Viking Shopping List
# VSL/templatetags/group_tags


from django import template
from django.contrib.auth.models import Group

register = template.Library()

@register.filter(name='has_group')
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False


#{% if request.user|has_group:"mygroup" %}

#{% endif %}

