# Peter AC Munro Python Portfolio
# Viking Shopping List
# VSL/Views

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError
from django.core.mail import send_mail, EmailMultiAlternatives
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import get_template, render_to_string
from django.urls import reverse
from django.views.decorators.http import require_POST
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode

import datetime

from .models import Event, Item, Attendance
from users.models import OptIn
from .forms import EventForm, VSLForm
from .tokens import coms_opt_token, attend_token


def index(request):
    """Home page"""
    return render(request, 'VSL/index.html')


@login_required
def events(request):
    """Show all events"""
    events = Event.objects.order_by('date_of')
    form = EventForm(request.POST or None)
    form.is_bound
    attending = Event.objects.filter(attendants__user=request.user, attendants__is_attending=True)

    context = {'events': events, 'form': form, 'attending':attending}
    return render(request, 'VSL/events.html', context)


@login_required
def event(request, event_id):
    """show the Event and shopping list."""
    event = get_object_or_404(Event, id=event_id)
    shopping_list = event.item_set.order_by('date_added')
    form = VSLForm()
    attendee_list = User.objects.filter(attending__event=event, attending__is_attending=True)

    context = {'event': event, 'shopping_list': shopping_list, 'form': form, 'attendee_list': attendee_list}
    return render(request, 'VSL/event.html', context)


@login_required
@require_POST
def new_event(request):
    """Add a new event."""
    # Check user permission
    if request.user.has_perm('VSL.add_event'):
        form = EventForm(request.POST)
        if form.is_valid():
            new_event = Event(title=request.POST['title'])
            new_event.date_of = form.cleaned_data.get('date_of')
            new_event.added_by = request.user
            new_event.save()
            make_attendance(new_event)
            notify_new_event(request, new_event)
            return redirect('VSL:events')
        else:
            return events(request)
    else:
        'done'


def make_attendance(new_event):
    """set user as attending an event."""
    event = new_event
    users = User.objects.all()
    for user in users:
        attendance = Attendance(
            user = user, 
            event = event,
            is_attending = False
        )
        attendance.save()



@login_required
def edit_event(request, event_id):
    """Edit the details of an existing event"""
    event = Event.objects.get(id=event_id)
    if request.user.has_perm('VSL.change_event'):
        # Get the existing event data
        if request.method != 'POST':
            form = EventForm(instance=event)
        else:
            form = EventForm(instance=event, data=request.POST)
            if form.is_valid():
                form.save()
                notify_edit_event(request, event)
                return HttpResponseRedirect(reverse('VSL:event', args=[event.id]))
    else:
        'done'

    context = {'event': event, 'form': form}
    return render(request, 'VSL/edit_event.html', context)


@login_required
def delete_event(request, event_id):
    """delete an item from the shopping list"""
    event = get_object_or_404(Event, id=event_id)
    if request.user.has_perm('VSL.delete_event'):
        event.delete()
        notify_delete_event(request, event)
        return redirect('VSL:events')
    else:
        return HttpResponseRedirect(reverse('VSL:event', args=[event.id]))



@login_required
def add_item(request, event_id):
    """Add an item to the viking shopping list"""
    event = get_object_or_404(Event, id=event_id)
    form = VSLForm(request.POST)
    if request.user.has_perm('VSL.add_item'):
        if form.is_valid():
            new_item = Item(name=request.POST['item'])
            new_item.event = event
            new_item.added_by = request.user
            new_item.save()
            notify_added(request, new_item)
        return HttpResponseRedirect(reverse('VSL:event', args=[event_id]))
    else:
        return HttpResponseRedirect(reverse('VSL:event', args=[event_id]))


@login_required
def pillage_item(request, item_id):
    """ Mark an item as pillaged (purchased)"""
    item = Item.objects.get(id=item_id)
    if request.user.has_perm('VSL.change_item'):
        item.is_pillaged = True
        item.pillaged_by = request.user
        item.save()
        notify_pillaged(request, item)
        return HttpResponseRedirect(reverse('VSL:event', args=[item.event_id]))
    else:
        return HttpResponseRedirect(reverse('VSL:event', args=[item.event_id]))


@login_required
def delete_item(request, item_id):
    """delete an item from the shopping list"""
    item = Item.objects.get(id=item_id)
    if request.user.has_perm('VSL.delete_item'):
        item.delete()
        notify_deleted_item(request, item)
        return HttpResponseRedirect(reverse('VSL:event', args=[item.event_id]))
    else:
        return HttpResponseRedirect(reverse('VSL:event', args=[item.event_id]))


@login_required
def is_attending(request, event_id):
    """set user as attending an event."""
    event = get_object_or_404(Event, id=event_id)
    attendance = Attendance.objects.get(
        user = request.user, 
        event = event,
        )
    attendance.is_attending = True
    attendance.save()
    return redirect('VSL:events')


@login_required
def not_attending(request, event_id):
    """set user as NOT attending an event."""
    event = get_object_or_404(Event, id=event_id)
    attendance = Attendance.objects.get(
        user = request.user, 
        event = event,
        )
    attendance.is_attending = False
    attendance.save()
    return redirect('VSL:events')


def attending_confirmed(request, event_id, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None:
        event = Event.objects.get(id=event_id)
        attend = Attendance.objects.get(
            user = user,
            event = event,
            )
        if attend_token.check_token(attend, token):
            attendance = attend
            attendance.is_attending = True
            attendance.save()
            return render(request, 'VSL/attending_confirmed.html')
        else:
            return render(request, 'users/invalid.html')
    else:
        return render(request, 'users/invalid.html')



def make_unsub_link(request, user):
    """create a link to unsubscribe the user from notifications"""
    domain = get_current_site(request).domain
    unsub_link = render_to_string('notifications/unsub_link.txt', {
                'user':user,
                'domain':domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token':coms_opt_token.make_token(user)
                })

    return unsub_link


def make_attend_link(request, event, user):
    """create a link to unsubscribe the user from notifications"""
    domain = get_current_site(request).domain
    attend = Attendance.objects.get(
            user = user, 
            event = event,
            )
    attend_link = render_to_string('notifications/attend_link.txt', {
                'user':user,
                'event':event,
                'domain':domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token':attend_token.make_token(attend)
                })

    return attend_link



def notify_added(request, new_item):
    item = new_item
    event = new_item.event
    users = User.objects.filter(opt_in__recieve_coms=True)
    site_name = get_current_site(request).name
    for user in users:
        if user.is_active and user != request.user and user.last_login != None:
            unsub_link = make_unsub_link(request, user)
            with open(settings.BASE_DIR + "/VSL/templates/notifications/item_added_email.txt") as t:
                ne_message = t.read()
                message = EmailMultiAlternatives(
                    subject = str(site_name) + " - " + str(event.title) + " - New item added.",
                    from_email = settings.EMAIL_HOST_USER,
                    to = [user.email],
                    )
                context = {'request':request, 'user':user, 'event':event, 'item':item, 'site_name':site_name, 'unsub_link':unsub_link}
                html_template = get_template("notifications/item_added_email.html").render(context)
                message.attach_alternative(html_template, "text/html")
                message.send()
        else:
            'done'


def notify_pillaged(request, item):
    item = item
    event = item.event
    users = User.objects.filter(opt_in__recieve_coms=True)
    site_name = get_current_site(request).name
    for user in users:
        if user.is_active and user != request.user and user.last_login != None:
            unsub_link = make_unsub_link(request, user)
            with open(settings.BASE_DIR + "/VSL/templates/notifications/item_pillaged_email.txt") as t:
                ne_message = t.read()
                message = EmailMultiAlternatives(
                    subject = str(site_name) + " - " + str(event.title) + " - Item pillaged.",
                    from_email = settings.EMAIL_HOST_USER,
                    to = [user.email],
                    )
                context = {'request':request, 'user':user, 'event':event, 'item':item, 'site_name':site_name, 'unsub_link':unsub_link}
                html_template = get_template("notifications/item_pillaged_email.html").render(context)
                message.attach_alternative(html_template, "text/html")
                message.send()
        else:
            'done'


def notify_deleted_item(request, item):
    event = item.event
    users = User.objects.filter(opt_in__recieve_coms=True)
    site_name = get_current_site(request).name
    for user in users:
        if user.is_active and user != request.user and user.last_login != None:
            unsub_link = make_unsub_link(request, user)
            with open(settings.BASE_DIR + "/VSL/templates/notifications/item_deleted_email.txt") as t:
                ne_message = t.read()
                message = EmailMultiAlternatives(
                    subject = str(site_name) + " - " + str(event.title) + " - Item deleted.",
                    from_email = settings.EMAIL_HOST_USER,
                    to = [user.email],
                    )
                context = {'request':request, 'user':user, 'event':event, 'item':item, 'site_name':site_name, 'unsub_link':unsub_link}
                html_template = get_template("notifications/item_deleted_email.html").render(context)
                message.attach_alternative(html_template, "text/html")
                message.send()
        else:
            'done'


def notify_new_event(request, new_event):
    event = new_event
    users = User.objects.filter(opt_in__recieve_coms=True)
    site_name = get_current_site(request).name
    for user in users:
        if user.is_active and user != request.user and user.last_login != None:
            unsub_link = make_unsub_link(request, user)
            attend_link = make_attend_link(request, event, user)
            with open(settings.BASE_DIR + "/VSL/templates/notifications/new_event_email.txt") as t:
                ne_message = t.read()
                message = EmailMultiAlternatives(
                    subject = str(site_name) + " - " + str(event.title) + " event has been created.",
                    from_email = settings.EMAIL_HOST_USER,
                    to = [user.email],
                    )
                context = {'request':request, 'user':user, 'event':event, 'site_name':site_name, 'unsub_link':unsub_link, 'attend_link':attend_link}
                html_template = get_template("notifications/new_event_email.html").render(context)
                message.attach_alternative(html_template, "text/html")
                message.send()
        else:
            'done'


def notify_edit_event(request, event):
    users = User.objects.filter(opt_in__recieve_coms=True)
    site_name = get_current_site(request).name
    for user in users:
        if user.is_active and user != request.user and user.last_login != None:
            unsub_link = make_unsub_link(request, user)
            with open(settings.BASE_DIR + "/VSL/templates/notifications/edit_event_email.txt") as t:
                ne_message = t.read()
                message = EmailMultiAlternatives(
                    subject = str(site_name) + " - " + str(event.title) + " event update.",
                    from_email = settings.EMAIL_HOST_USER,
                    to = [user.email],
                    )
                context = {'request':request, 'user':user, 'event':event, 'site_name':site_name, 'unsub_link':unsub_link}
                html_template = get_template("notifications/edit_event_email.html").render(context)
                message.attach_alternative(html_template, "text/html")
                message.send()
        else:
            'done'


def notify_delete_event(request, event):
    users = User.objects.filter(opt_in__recieve_coms=True)
    site_name = get_current_site(request).name
    for user in users:
        if user.is_active and user != request.user and user.last_login != None:
            unsub_link = make_unsub_link(request, user)
            with open(settings.BASE_DIR + "/VSL/templates/notifications/delete_event_email.txt") as t:
                ne_message = t.read()
                message = EmailMultiAlternatives(
                    subject = str(site_name) + " - " + str(event.title) + " event cancelled.",
                    from_email = settings.EMAIL_HOST_USER,
                    to = [user.email],
                    )
                context = {'request':request, 'user':user, 'event':event, 'site_name':site_name, 'unsub_link':unsub_link}
                html_template = get_template("notifications/delete_event_email.html").render(context)
                message.attach_alternative(html_template, "text/html")
                message.send()
        else:
            'done'

