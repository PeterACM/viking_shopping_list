# Peter AC Munro Python Portfolio
# Viking Shopping List
# VSL/Admin

from django.contrib import admin

from VSL.models import Event, Item

admin.site.register(Event)
admin.site.register(Item)

