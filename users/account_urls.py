# Peter AC Munro Python Portfolio
# Viking Shopping List
# user/URLs

"""User App URL Configuration"""

from django.urls import path
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

from . import views


urlpatterns = [
    # Allow a logged out user to request a password change email
    path('password_reset/', auth_views.PasswordResetView.as_view(template_name='users/reset_password.html'), name='password_reset'),
    # Inform that password reset email has been generated and sent
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='users/reset_done.html'), name='password_reset_done'),
    # Enter and Confirm new password
    path('password_reset/confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='users/reset_confirm.html'), name='password_reset_confirm'),
    # Inform that password reset is complete
    path('password_reset/complete/', auth_views.PasswordResetCompleteView.as_view(template_name='users/reset_complete.html'), name='password_reset_complete'),
    # An alternate template 
    path('welcome/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='users/welcome.html'), name='welcome'),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

