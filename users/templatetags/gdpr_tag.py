# Peter AC Munro Python Portfolio
# Viking Shopping List
# VSL/templatetags/gdpr_tag


from django import template
from users.models import OptIn

register = template.Library()

@register.filter(name='gdpr_accept')
def gdpr_accept(user):
    x = OptIn.objects.get(user=user).gdpr
    if x == False:
        return True
    else:
       return False


#{% if request.user|gdpr_accpet %}

#{% endif %}

