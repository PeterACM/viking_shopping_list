# Peter AC Munro Python Portfolio
# Viking Shopping List
# user/URLs

"""User App URL Configuration"""

from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from . import views


app_name = 'users'
urlpatterns = [
    # User Admin - List Users, group they are in, Active status
    path('profiles/', views.profiles, name='profiles'),
    # Create a new user
    path('create_user/', views.create_user, name='create_user'),
    # Show and Edit Own user details
    path('my_profile/', views.my_profile, name='my_profile'),
    # Allow a logged in user to change their own password
    path('change_password/', views.change_password, name='change_password'),
    # Accept GDPR
    path('gdpr_confirm/', views.gdpr_confirm, name='gdpr_confirm'),
    # Recieve Emails
    path('coms_confirm/', views.coms_confirm, name='coms_confirm'),
    # Cancel Emails
    path('coms_cancel/', views.coms_cancel, name='coms_cancel'),
    # Cancel Emails from email
    path('unsubscribe/<uidb64>/<token>/', views.unsubscribe, name='unsubscribe'),
    ]

