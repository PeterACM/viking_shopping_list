# Peter AC Munro Python Portfolio
# Viking Shopping List
# Users/Models

from django.db import models
from django.contrib.auth.models import User


class OptIn(models.Model):
    """User confirmation of acceptance of site storing email address"""
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='opt_in')
    gdpr = models.BooleanField(default=False)
    recieve_coms = models.BooleanField(default=True)

