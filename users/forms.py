# Peter AC Munro Python Portfolio
# Viking Shopping List
# user/forms


from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User, Group



class AddUserForm(forms.ModelForm):
    email = forms.EmailField(required=True)
    group = forms.ModelChoiceField(queryset=Group.objects.all(), required=True)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'group']

    def clean_first_name(self):
        if self.cleaned_data['first_name'].strip() == '':
            raise ValidationError("First name is required.")
        return self.cleaned_data['first_name']

    def clean_last_name(self):
        if self.cleaned_data['last_name'].strip() == '':
            raise ValidationError("Last name is required.")
        return self.cleaned_data['last_name']

    def save(self, commit=True):
        user = super(AddUserForm, self).save(commit=False)
        randompass = User.objects.make_random_password()
        group = self.cleaned_data['group']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.password1 = randompass
        user.password2 = randompass

        if commit:
            user.save()
            user.groups.add(group)

        return user


class UserOwnForm(forms.ModelForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email']

    def clean_first_name(self):
        if self.cleaned_data['first_name'].strip() == '':
            raise ValidationError("First name is required.")
        return self.cleaned_data['first_name']

    def clean_last_name(self):
        if self.cleaned_data['last_name'].strip() == '':
            raise ValidationError("Last name is required.")
        return self.cleaned_data['last_name']

    def save(self, commit=True):
        user = super(UserOwnForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user






