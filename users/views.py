# Peter AC Munro Python Portfolio
# Viking Shopping List
# user/Views

import string, random
from django.conf import settings
from django.contrib.auth import login, authenticate, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm, PasswordResetForm
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.mail import send_mail
from django.forms import modelform_factory, modelformset_factory
from django.http import HttpRequest, HttpResponseRedirect, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.decorators.http import require_POST

from .forms import AddUserForm, UserOwnForm

from .models import OptIn
from VSL.models import Event, Attendance
from VSL.tokens import coms_opt_token


def is_group(user):
    return user.groups.filter(name='Jarls').exists()


@login_required
@user_passes_test(is_group)
def profiles(request):
    """Show all users details"""
    userformset = modelformset_factory(User, fields=('first_name', 'last_name', 'username', 'groups',), extra=0)
    if request.method != 'POST':
        formset = userformset(queryset=User.objects.filter(is_superuser=False).order_by('last_name'))
    else:
        formset = userformset(request.POST)
        if formset.is_valid():
            formset.save()

    context = {'formset': formset}
    return render(request, 'users/profiles.html', context)


@login_required
@user_passes_test(is_group)
def create_user(request):
    """Create a new user"""
    if request.method != 'POST':
        form = AddUserForm()
    else:
        form = AddUserForm(request.POST)
        if form.is_valid():
            new_user_email = form.cleaned_data.get('email')
            new_user = form.save()
            new_optin = OptIn(user=new_user, recieve_coms=True, gdpr=False)
            new_optin.save()
            notify_new_user(new_user_email)
            return redirect('users:profiles')

    context = {'form': form}
    return render(request, 'users/create_user.html', context)


@login_required
def my_profile(request):
    """A page for a user to edit their own details"""
    attending_list = Event.objects.filter(attendants__user=request.user, attendants__is_attending=True)
    recieve_coms = OptIn.objects.get(user=request.user).recieve_coms
    if request.method != 'POST':
        form = UserOwnForm(instance=request.user)
    else:
        form = UserOwnForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('users:my_profile')

    context = {'userownform': form, 'attending_list':attending_list, 'recieve_coms': recieve_coms}
    return render(request, 'users/my_profile.html', context)


@login_required
def gdpr_confirm(request):
    """A function to enable the user to confirm use of email"""
    gdproptin = OptIn.objects.get(user=request.user)
    gdproptin.gdpr = True

    gdproptin.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))



@login_required
def coms_confirm(request):
    """Set user as recieving email updates"""
    comsoptin = OptIn.objects.get(user=request.user)
    comsoptin.recieve_coms = True

    comsoptin.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
def coms_cancel(request):
    """set user as not receiving any email updates"""
    comsoptin = OptIn.objects.get(user=request.user)
    comsoptin.recieve_coms = False

    comsoptin.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def unsubscribe(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and coms_opt_token.check_token(user, token):
        comsoptin = user.opt_in
        comsoptin.recieve_coms = False
        comsoptin.save()
        return render(request, 'users/unsubscribe.html')
    else:
        return render(request, 'registration/invalid.html')


@login_required
def change_password(request):
    """A page for the user to change their password"""
    if request.method != 'POST':
        form = PasswordChangeForm(user=request.user)
    else:
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('users:my_profile')

    context = {'changepassform': form}
    return render(request, 'users/change_password.html', context)



def notify_new_user(new_user_email):
    """sends a password reset email to a newly created user."""
    subject = "Welcome to the Viking shopping list! Now complete your registration"
    form = PasswordResetForm({'email': new_user_email})
    if form.is_valid():
        request = HttpRequest()
        request.META['SERVER_NAME'] = 'vikingshoppinglist.herokuapp.com'
        request.META['SERVER_PORT'] = '80'
#        request.META['SERVER_NAME'] = 'localhost'
#        request.META['SERVER_PORT'] = '8000'
        form.save(
            request=request,
            use_https=True,
            from_email=settings.DEFAULT_FROM_EMAIL,
            email_template_name='users/new_user_email.txt',
            html_email_template_name='users/new_user_email.html',
            subject_template_name = 'users/new_user_subject.txt',
            )

    return 'done'


