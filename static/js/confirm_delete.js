// Peter AC Munro Python Portfolio
// Viking Shopping List
// static/confirm_delete


$(document).on('click', '.confirm-delete', function () {
    $("#ConfirmDeleteModal").attr("caller-id", $(this).attr("id"));
    console.log();
});

$(document).on('click', '#ConfirmDeleteButtonModal', function () {
    var caller = $("#ConfirmDeleteButtonModal").closest(".modal").attr("caller-id");
    window.location = $("#".concat(caller)).attr("href");
    console.log();
});

